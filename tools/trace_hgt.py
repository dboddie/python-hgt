#!/usr/bin/env python

"""
Copyright (C) 2014 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import sys

from hgt import HGTFile, HGT_Undefined

# Turning left:  0, 1 -> 1, 0 -> 0, -1 -> -1, 0
# Turning right: 0, 1 -> -1, 0 -> 0, -1 -> 1, 0

def follow(data, x0, x1, y0, y1, sx, sy, sdx, sdy, threshold):

    # Follow the contour, keeping values below the threshold on the left, until
    # we reach the edge of the tile.
    segments = []
    last = None
    x, y = sx, sy
    dx, dy = sdx, sdy
    
    while True:
    
        nx = x + dx
        ny = y + dy
        
        # If the next move causes the piece to leave the map area then break.
        if not x0 <= nx <= x1 or not y0 <= ny <= y1:
            segments.append((dx, dy))
            break
        
        v = data[ny][nx]
        
        if v <= threshold and v != HGT_Undefined:
            # The way is blocked. Turn to the right.
            segments.append((dx, dy))
            last = (dx, dy)
            dx, dy = -dy, dx
            continue
        
        if (dx, dy) == last:
            segments[-1] = (segments[-1][0] + dx, segments[-1][1] + dy)
        else:
            segments.append((dx, dy))
        
        last = (dx, dy)
        
        x = nx
        y = ny
        
        # Check to the left of the new position.
        v = data[y - dx][x + dy]
        
        if v > threshold or v == HGT_Undefined:
            # An empty space. Turn left.
            dx, dy = dy, -dx
            x += dx
            y += dy
    
    if segments:
    
        # If the line started in the next cell then trim the start of it.
        if sdx == -1:
            dx, dy = segments[0]
            dx += 1
            if dx != 0 or dy != 0:
                segments[0] = (dx, dy)
            else:
                segments.pop(0)
        
        elif sdy == -1:
            dx, dy = segments[0]
            dy += 1
            if dx != 0 or dy != 0:
                segments[0] = (dx, dy)
            else:
                segments.pop(0)
        
        # If the last position is in the next cell (taking into account that the
        # extra column and row are technically in the next cell but their edges
        # are adjacent to this cell) then discard it.
        if x >= x1 and segments[-1][0] == 1:
            segments.pop()
        elif y >= y1:
            segments.pop()
    
    # Append the last position and direction to the end of the line if there
    # are any segments. This allows us to join lines together later.
    if segments:
        lx, ly = sx, sy
        for dx, dy in segments:
            lx += dx
            ly += dy
        segments.append((lx, ly))
    
    return segments


def test_edge(data, p0, p1, dp, x0, y0, x1, y1, threshold, lines, outside_edge = False):

    v0 = HGT_Undefined
    
    ox = 0
    oy = 0
    
    # When traversing from right to left on the top edge, the boundaries of the
    # cells found will be on the right-hand side of each cell. Similarly, when
    # traversing from bottom to top on the right edge, the boundaries of the
    # cells will be on the bottom of each cells. The offset reflects this
    # starting offset.
    if dp[0] == -1:
        ox = 1
    elif dp[1] == -1:
        oy = 1
    
    x, y = p0
    lx, ly = p0
    
    while (x, y) != p1:
    
        if v0 == HGT_Undefined:
            v = data[y][x]
            
            if v != HGT_Undefined:
                v0 = v
        
        x += dp[0]
        y += dp[1]
        
        if v0 == HGT_Undefined:
            continue
        
        v1 = data[y][x]
        
        if v1 == HGT_Undefined:
            continue
        
        v1 -= threshold
        
        # If we cross the threshold from below, start tracing a line to the
        # left of our path along the edge we are following.
        if v0 * v1 <= 0:
        
            if v1 > 0:
            
                if outside_edge:
                    # Only collect the position of the change.
                    lx, ly = x, y
                else:
                    dx, dy = dp[1], -dp[0]
                    segments = follow(data, x0, x1, y0, y1, x, y, dx, dy, threshold)
                    
                    if segments:
                        lx, ly = segments[-1]
                        segments[-1] = (lx + ox, ly + oy)
                        #print (x + ox, y + oy), "...", segments[-1]
                        
                        if (x + ox, y + oy) in lines:
                            print "Replacing line of length", len(lines[(x, y)]), "at", (x, y), "with line of length", len(segments)
                            print lines[(x, y)]
                            print
                            print segments
                            print
                        lines[(x + ox, y + oy)] = segments
            
            elif v0 > 0 and outside_edge:
                lines[(x + ox, y + oy)] = [(lx - x, ly - y), (lx + ox, ly + oy)]
        
        v0 = v1


def merge_lines(lines):

    # Merge lines by looking up the ends of each line in the keys of the lines
    # dictionary.
    
    starts = lines.keys()
    
    for start in starts:
    
        # Skip any lines that have already been merged into others.
        if start not in lines:
            continue
        
        x, y = start
        
        segments = lines[start]
        end = segments[-1]
        
        while end != start and end in lines:
        
            # Only remove the end item if it can be replaced by a list with a
            # new one.
            end = segments.pop()
            
            # As long as there is another line that connects to this one, find
            # it and remove it from the lines dictionary.
            next = lines[end]
            del lines[end]
            
            # Update the end point with the one from the next line and append
            # the segments to the current list.
            segments += next
            end = segments[-1]
        
        lines[start] = segments
    
    print "Merged", len(starts) - len(lines), "lines."
    
    # Remove the end items from all of the lines.
    
    for start, segments in lines.items():
    
        segments.pop()
        lines[start] = segments


def write_svg(svg_file, width, height, hsteps, vsteps, tw, th, lines, plot_cells = False):

    f = open(svg_file, "w")
    f.write('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>\n'
            '<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN"\n'
            '  "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">\n')
    
    f.write('<svg version="1.1"\n'
            '     xmlns="http://www.w3.org/2000/svg"\n'
            '     xmlns:xlink="http://www.w3.org/1999/xlink"\n'
            '     width="%fcm" height="%fcm"\n'
            '     viewBox="0 0 %i %i">\n' % (width/100.0, height/100.0, width, height))
    
    f.write('<rect x="0" y="0" width="%i" height="%i"' % (width, height))
    f.write('      stroke="none" fill="#000040" />\n')
    
    if plot_cells:
    
        data, imin, imax, jmin, jmax = plot_cells
        
        i = imin * th
        while i < ((imax + 1) * th):
        
            row = data[i]
            j = jmin * tw
            while j < ((jmax + 1) * tw):
            
                f.write('<rect x="%i" y="%i" width="1" height="1" ' % (j, i))
                if -32768 < row[j] <= 0:
                    f.write(' stroke="none" fill="#8080ff" />\n')
                else:
                    f.write(' stroke="none" fill="#80ff80" />\n')
                
                j += 1
            
            i += 1
    
    i = 0
    while i <= vsteps:
        f.write('<path d="M %f %f L %f %f" ' % (0, i * th, width, i * th))
        f.write('stroke="#404040" fill="none" stroke-width="0.125" />\n')
        i += 1
    
    j = 0
    while j <= hsteps:
        f.write('<path d="M %f %f L %f %f" ' % (j * th, 0, j * th, height))
        f.write('stroke="#404040" fill="none" stroke-width="0.125" />\n')
        j += 1
    
    for start, segments in lines.items():
    
        x, y = start
        
        f.write('<path d="M %f %f ' % (x, y))
        
        for segment in segments:
            f.write('l %i %i ' % segment)
            x, y = x + segment[0], y + segment[1]
        
        while x != start[0] and y != start[1]:
        
            # Add points to lines that start and end on different edges so that
            # their filled area extends to the edges of the map.
            break
        
        f.write('z"\n      ')
        f.write('stroke="black" fill="#408040" stroke-width="0.25" />\n')
    
    f.write('</svg>\n')


if __name__ == "__main__":

    args = sys.argv[:]
    
    if len(args) != 5:
        sys.stderr.write("Usage: %s <hgt file> <SVG file> <number of divisions> <threshold>\n" % sys.argv[0])
        sys.exit(1)
    
    h = HGTFile(unicode(args[1]))
    data = h.read()
    
    # The last row and last column are duplicates of the first row and first
    # column in the adjacent files in the dataset, so we only use those to help
    # with alignment. Similarly, we include an extra row and column when we
    # create subtiles of this file.
    width = height = len(data) - 1
    
    svg_file = args[2]
    divisions = int(args[3])
    threshold = int(args[4])
    
    hsteps = vsteps = 2**divisions
    tw = width/hsteps
    th = height/vsteps
    
    # Traverse the edges of each tiles within the data.
    
    lines = {}
    grid_lines = []
    
    i = 0
    while i < vsteps:
    
        # Include the row beneath this subtile.
        y0 = i * th
        y1 = ((i + 1) * th)
        
        j = 0
        while j < hsteps:
        
            # Include the column to the right of this subtile.
            x0 = j * tw
            x1 = ((j + 1) * tw)
            
            test_edge(data, (x1, y0), (x0, y0), (-1, 0), x0, y0, x1, y1, threshold, lines)
            test_edge(data, (x0, y0), (x0, y1), (0, 1), x0, y0, x1, y1, threshold, lines)
            test_edge(data, (x0, y1), (x1, y1), (1, 0), x0, y0, x1, y1, threshold, lines)
            test_edge(data, (x1, y1), (x1, y0), (0, -1), x0, y0, x1, y1, threshold, lines)
            
            j += 1
        
        i += 1
    
    test_edge(data, (width, 0), (0, 0), (-1, 0), None, None, None, None, threshold, lines, True)
    test_edge(data, (0, 0), (0, height), (0, 1), None, None, None, None, threshold, lines, True)
    test_edge(data, (0, height), (width, height), (1, 0), None, None, None, None, threshold, lines, True)
    test_edge(data, (width, height), (width, 0), (0, -1), None, None, None, None, threshold, lines, True)
    
    merge_lines(lines)
    
    write_svg(svg_file, width, height, hsteps, vsteps, tw, th, lines)
    #write_svg(svg_file, width, height, hsteps, vsteps, tw, th, lines,
    #          plot_cells = (data, 4, 5, 6, 7))
    
    #sys.exit()
