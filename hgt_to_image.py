#!/usr/bin/env python

"""
Copyright (C) 2014 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import sys

from hgt import HGTFile

#from PyQt4.QtGui import QApplication, QImage, qRgb
import Image

if __name__ == "__main__":

    #app = QApplication(sys.argv)
    #args = app.arguments()
    args = sys.argv[:]
    
    if len(args) != 3:
        sys.stderr.write("Usage: %s <hgt file> <image file>\n" % sys.argv[0])
        sys.exit(1)
    
    h = HGTFile(unicode(args[1]))
    data = h.read()
    
    width = height = len(data)
    #im = QImage(width, height, QImage.Format_RGB32)
    s = ""
    
    maxes = []
    
    for row in data:
        maxes.append(max(filter(lambda h: h != 32768, map(abs, row))))
    
    scale = max(maxes)
    
    y = 0
    for row in data:
    
        x = 0
        for h in row:
        
            if h == -32768:
                r = 255
                g = b = 0
            else:
                v = abs(float(h)/scale)
                if h > 0:
                    r = b = int(v * 255)
                    g = 64 + int(v * 191)
                else:
                    r = g = 0
                    b = int(v * 128)
            
            #im.setPixel(x, y, qRgb(r, g, b))
            s += chr(r) + chr(g) + chr(b)
            x += 1
        
        y += 1
    
    #im.save(unicode(args[2]))
    im = Image.fromstring("RGB", (width, height), s)
    im.save(args[2])
    sys.exit()
